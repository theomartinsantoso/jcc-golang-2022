package main

import "fmt"

func main() {
	//soal 1
	fmt.Println("===== SOAL 1 ======")
	var luasLingkaran float64
	var kelilingLingkaran float64
	fmt.Println(luasLingkaran)
	fmt.Println(kelilingLingkaran)

	//soal 2
	func introduce(nama, gender, work, umur string)(sentence *string){
		prefix := "Bu"
		if gender == "laki-laki" {
			prefix = "Pak"
		}
		*sentence = fmt.Sprintf("%d %d adalah seorang %d yang berusia %d tahun", prefix, nama, work, umur)
		return
	}

	func main(){
		var sentence string
		introduce(&sentence, "John", "laki-laki", "penulis", "30")

		fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
		introduce(&sentence, "Sarah", "perempuan", "model", "28")

		fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"
	}

	//soal 3
	fmt.Println("===== SOAL 3 =====")

	var buah = []string{}
	var varianbuah = append(buah, "Jeruk", "Semangka", "Mangga", "Strawberry", "Durian", "Maggis", "Alpukat")
	var varianbuah2 *[]string = &varianbuah

	fmt.Println(varianbuah2)

	//soal 4
	fmt.Println("===== SOAL 4 =====")

	func tambahDataFilm(dataFilm *[]map[string]string, title, jam, genre, tahun string){
		data := map[string]string{
			"title": title,
			"jam": jam,
			"genre": genre,
			"tahun": tahun,
		}
		dataFilm = append(dataFilm, film)
	}
	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

	for _, item := range dataFilm {
		fmt.Println(item)
}
